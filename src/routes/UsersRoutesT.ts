import { idInParmsModel, userCreationModelT } from '../validations/Validations';
import * as Hapi from '@hapi/hapi';
import { Response } from '../classes/Response';
import { StatusCodes } from '../classes/StatusCodes';
import UserControllerT from '../controllers/UserControllerT';

export default function (server: Hapi.Server) {

    const controller = new UserControllerT(server);

    server.bind(controller);

    server.route({
        method: 'POST',
        path: '/add-user',
        options: {
            handler: controller.createUser,
            tags: ['api'],
            description: 'Create User Api',
            auth: false,
            validate: {
                payload: userCreationModelT,
                failAction: async (request, h, err) => {

                    if (err) {

                        let response = new Response(false, StatusCodes.NOT_ACCEPTABLE, err.message, {});
                        return h.response(response).code(response.getStatusCode()).takeover();

                    } else {

                        return h.continue;
                    }
                }
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            'description': 'user Created.'
                        },
                        '406': {
                            'description': 'Validation Error.'
                        }
                    }
                }
            }
        }
    });

    server.route({
        method: 'get',
        path: '/get-all-users',
        options: {
            handler: controller.getAllUsers,
            tags: ['api'],
            description: 'Get users Api',
            auth: false,
            validate: {
                failAction: async (request, h, err) => {

                    if (err) {

                        let response = new Response(false, StatusCodes.NOT_ACCEPTABLE, err.message, {});
                        return h.response(response).code(response.getStatusCode()).takeover();

                    } else {

                        return h.continue;
                    }
                }
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            'description': 'Resource fetched.'
                        },
                        '406': {
                            'description': 'Validation Error.'
                        }
                    }
                }
            }
        }
    });

    server.route({
        method: 'PUT',
        path: '/update-user/{id}',
        options: {
            handler: controller.updateUser,
            tags: ['api'],
            description: 'Update admin Api',
            auth: false,
            validate: {
                params: idInParmsModel,
                failAction: async (request, h, err) => {

                    if (err) {

                        let response = new Response(false, StatusCodes.NOT_ACCEPTABLE, err.message, {});
                        return h.response(response).code(response.getStatusCode()).takeover();

                    } else {

                        return h.continue;
                    }
                }
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            'description': 'user role updated Updated.'
                        },
                        '406': {
                            'description': 'Validation Error.'
                        }
                    }
                }
            }
        }
    });

}