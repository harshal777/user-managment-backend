import * as Joi from "joi";
import { RouteOptionsResponseSchema } from "@hapi/hapi";

export const idInParmsModel: RouteOptionsResponseSchema = <RouteOptionsResponseSchema>Joi.object().keys({
    id: Joi.string().required()
});

export const userCreationModelT: RouteOptionsResponseSchema = <RouteOptionsResponseSchema>Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required(),
    mobile: Joi.string().required(),
    gender: Joi.string().required(),
});

