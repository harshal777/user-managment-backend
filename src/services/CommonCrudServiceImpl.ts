"use strict";
import { Response } from '../classes/Response'
import { camelizeKeys, decamelizeKeys, decamelize } from 'humps'
import { StatusCodes } from '../classes/StatusCodes';
import { CustomMessages } from '../classes/CustomMessages';

export default class CommonCrudServiceImpl {
    async
    private dbModel;
    private notReturningKeysInCollection = {
        logs: 0,
        creted_by: 0,
        updated_by: 0,
        created_at: 0,
        updated_at: 0,
        v: 0,
        password: 0,
        is_deleted: 0,
        last_otp: 0,
        is_verified: 0,
        random_key: 0
    }


    constructor(dbModel: any) {
        this.dbModel = dbModel;
    }

    public async camalizeKeysForMongoArray(array) {

        return array.map(function (question) {

            let a = question.toJSON();

            let id = a['_id'];

            let b = camelizeKeys(a);

            b['id'] = id;

            return b;

        });

    }

    public async  createEntry(payload) {

        let response: Response;

        try {

            let cretaedEntry = await this.dbModel.create(decamelizeKeys(payload));

            let condition = {};
            condition['_id'] = cretaedEntry['_id'];

            let list = await this.dbModel.find(condition);
            list = await this.camalizeKeysForMongoArray(list);

            response = new Response(true, StatusCodes.CREATED, CustomMessages.CREATED, list);

        } catch (err) {

            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return response;
    }

    public async createOrFindEntry(payload, condition) {

        let response: Response;

        try {

            let entry = await this.dbModel.find(condition);

            if (entry.length === 0) {

                let cretaedEntry = await this.dbModel.create(decamelizeKeys(payload));
                let resourceEntry = await this.dbModel.find(condition);

                resourceEntry = await this.camalizeKeysForMongoArray(resourceEntry);
                response = new Response(true, StatusCodes.CREATED, CustomMessages.CREATED, resourceEntry);

            } else {
                response = new Response(false, StatusCodes.NOT_ACCEPTABLE, CustomMessages.ALREADY_PRESENT, {});
            }

        } catch (err) {

            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return response;
    }

    public async deleteEntry(id: String, logObj: any): Promise<Response> {

        let response: Response;
        try {

            let query = { "_id": id }

            let isEntryPresent = await this.dbModel.findById(id);

            if (!isEntryPresent) {

                response = new Response(false, StatusCodes.BAD_REQUEST, CustomMessages.NOT_PRESENT, {});
            } else {

                let updated_obj = {
                    "$set": {
                        "is_deleted": 1
                    }
                }

                if (isEntryPresent['logs'] !== undefined && Object.keys(logObj).length != 0) {

                    let logs = isEntryPresent['logs'];
                    logs.push(logObj);
                    updated_obj['logs'] = logs;
                } else {
                    updated_obj['logs'].push(logObj);
                }

                await this.dbModel.update(query, updated_obj);
                response = new Response(true, StatusCodes.OK, CustomMessages.DELETED, {});
            }
        } catch (err) {

            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return response;
    }

    public async getAllEntries(query: any, queryWhereCondition: any) {

        let response: Response;
        try {


            let sort = "";

            if (query['sort'])
                sort = decamelize(query['sort']);
            else
                sort = '-created_at'

            let length = await this.dbModel.find(decamelizeKeys(queryWhereCondition)).countDocuments();

            if (length !== 0) {

                let list = await this.dbModel.find(decamelizeKeys(queryWhereCondition), this.notReturningKeysInCollection);
                list = await this.camalizeKeysForMongoArray(list);

                let obj = {}
                obj['list'] = list;
                obj['count'] = length;

                response = new Response(true, StatusCodes.OK, CustomMessages.SUCCESS, obj);
            } else {

                let obj = {}
                obj['list'] = [];
                obj['count'] = 0
                response = new Response(true, StatusCodes.OK, CustomMessages.SUCCESS, obj);
            }
        } catch (err) {

            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return response;
    }


    public async getSingleEntry(id: string, keys?): Promise<Response> {
        if (keys && keys.length > 0) {
            for (const key of keys) {
                this.notReturningKeysInCollection[key] = 0;
            }
        }
        let response: Response;
        try {

            let condition = {};
            condition['_id'] = id;

            let list = await this.dbModel.find(condition, this.notReturningKeysInCollection);
            list = await this.camalizeKeysForMongoArray(list);
            response = new Response(true, StatusCodes.OK, CustomMessages.SUCCESS, list);
        } catch (err) {

            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return response;
    }

    public async updateEntry(id: string, payload: any, logObj): Promise<Response> {

        let response: Response;
        try {
            let condition = {};
            condition['_id'] = id;
            let isEntryPresent = await this.dbModel.find((condition));
            if (isEntryPresent.length !== 0) {

                if (isEntryPresent[0].logs !== undefined && Object.keys(logObj).length != 0) {

                    let logs = isEntryPresent[0].logs;
                    if (logObj.email) {
                        logs.push(logObj);
                    }
                    payload['logs'] = logs;
                } else {
                    if (logObj.email) {
                        payload['logs'] = [];
                        payload['logs'].push(logObj);
                    }

                }

                let a = await this.dbModel.updateOne(condition, decamelizeKeys(payload));

                let updatedEntry = await this.dbModel.find(condition, this.notReturningKeysInCollection);
                updatedEntry = await this.camalizeKeysForMongoArray(updatedEntry);
                response = new Response(true, StatusCodes.OK, CustomMessages.UPDATED, updatedEntry);

            } else {

                response = new Response(false, StatusCodes.BAD_REQUEST, CustomMessages.NOT_PRESENT, {});

            }
        } catch (err) {
            console.log('error', err);
            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return response;
    }


    public async updateUserEntry(id: string, payload: any): Promise<Response> {

        let response: Response;
        try {
            let condition = {};
            condition['_id'] = id;
            let isEntryPresent = await this.dbModel.find((condition));
            if (isEntryPresent.length !== 0) {

                let a = await this.dbModel.updateOne(condition, decamelizeKeys(payload));
                let updatedEntry = await this.dbModel.find(condition, this.notReturningKeysInCollection);
                updatedEntry = await this.camalizeKeysForMongoArray(updatedEntry);
                response = new Response(true, StatusCodes.OK, CustomMessages.UPDATED, updatedEntry);

            } else {

                response = new Response(false, StatusCodes.BAD_REQUEST, CustomMessages.NOT_PRESENT, {});

            }
        } catch (err) {
            console.log('error', err);
            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return response;
    }
}
