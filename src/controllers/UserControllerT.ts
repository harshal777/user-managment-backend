import * as Hapi from "@hapi/hapi";
import CommonCrudServiceImpl from "../services/CommonCrudServiceImpl";
import { DatabaseModelsInterface } from "..";
import { StatusCodes } from "../classes/StatusCodes";
import { Response } from "../classes/Response";
import { CustomMessages } from '../classes/CustomMessages';

export default class UserController {
    private userService: CommonCrudServiceImpl;

    constructor(server: Hapi.Server) {
        let mongo: DatabaseModelsInterface = global["mongo"];
        this.userService = new CommonCrudServiceImpl(mongo.UserModelT);
    }

    public async createUser(request: Hapi.Request, h: Hapi.ResponseToolkit) {
        let response: Response;

        try {

            let condition: any = {};
            condition["email"] = request.payload["email"];
            response = await this.userService.createOrFindEntry(request.payload, condition);
            let result = response.getResult();

            if (result && result.length > 0) {

                response = new Response(true, StatusCodes.OK, CustomMessages.SUCCESS, result);

            } else {

                response = new Response(false, StatusCodes.UNAUTHORIZED, CustomMessages.ALREADY_PRESENT, {});
            }
        } catch (err) {
            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return h.response(response).code(response.getStatusCode());
    }

    public async getAllUsers(request: Hapi.Request, h: Hapi.ResponseToolkit) {

        let response: Response;

        try {
            let queryWhereCondition = {};

            response = await this.userService.getAllEntries(request.query, queryWhereCondition);
            let result = response.getResult();

            if (result['list'] && result['list'].length !== 0) {
                response.setResult(result);
            } else {
                response.setResult([]);
            }
        } catch (err) {

            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
        }

        return h.response(response).code(response.getStatusCode());
    }

    public async updateUser(request: Hapi.Request, h: Hapi.ResponseToolkit) {
        let response: Response;

        try {
            const isExist = await this.userService.getSingleEntry(request.params['id']);

            if (isExist && isExist.getResult() && isExist.getResult().length > 0) {

                const payload = request.payload;
                response = await this.userService.updateUserEntry(request.params['id'], payload);

            } else {

                response = new Response(false, StatusCodes.BAD_REQUEST, CustomMessages.NOT_PRESENT, {});

            }

        } catch (err) {

            response = new Response(false, StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});

        }
        return h.response(response).code(response.getStatusCode());
    }

}
