import * as Hapi from '@hapi/hapi';
import { Response } from './classes/Response';
import { StatusCodes } from './classes/StatusCodes';
import { CustomMessages } from './classes/CustomMessages';
import { ServerConfigurationInterface } from './interfaces/config-interfaces/ServerConfigurationInterface';
import UsersRoutesT from './routes/UsersRoutesT';

export async function init(
    serverConfig: ServerConfigurationInterface,
    globalVariables: any,
): Promise<Hapi.Server> {

    try {
        const port = serverConfig.port;

        const server = new Hapi.Server({
            host: "0.0.0.0",
            port: port,
            router: {
                stripTrailingSlash: true,
            },
            routes: {
                cors: {
                    origin: ['*'],
                    credentials: true
                }
            },
            state: {
                strictHeader: false
            }
        });


        if (serverConfig.routePrefix) {
            server.realm.modifiers.route.prefix = serverConfig.routePrefix;
        }

        // Plugin Registration
        let hostString = global['host'];

        console.log("host string", hostString);

        await server.register([
            require('@hapi/inert'),
            require('@hapi/vision')
        ])


        // await Promise.all(pluginPromises);
        console.log('Register Routes');


        const scheme = function (server, options) {

            return {
                api: {
                    settings: {
                        x: 5
                    }
                },
                authenticate: async function (request, h) {
                    let token;
                    // check for basic auth header
                    if (!request.headers.authorization) {
                        let response = new Response(false, StatusCodes.UNAUTHORIZED, CustomMessages.MISSING_AUTH_HEADER, {})
                        return h.response(response).code(response.getStatusCode()).takeover();
                    }

                }
            };
        };

        server.auth.scheme('custom', scheme);
        server.auth.strategy('default', 'custom');
        server.auth.default('default');

        // Routes Registration
        UsersRoutesT(server);
        console.log('Routes registered sucessfully.');

        return server;
    } catch (err) {

        console.log('Error starting server: ', err);
        throw err;
    }
}
