import * as Mongoose from "mongoose";

export interface UserInterfceT extends Mongoose.Document {
    name: string;
    email: string;
    mobile: string;
    gender: string;
}