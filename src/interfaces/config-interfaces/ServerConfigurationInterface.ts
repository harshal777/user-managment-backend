
export interface ServerConfigurationInterface {    
    port: number;
    routePrefix: string,
    swaggerBasePath: string,
    SwaggerSchemes: string
}
