import { ServerConfigurationInterface } from "./ServerConfigurationInterface";
import { MongoConfigurationInterface } from "./MongoConfigurationInterface";

export interface ConfigurationSettingsInterface {

    serverConfig: ServerConfigurationInterface,
    mongoConfig: MongoConfigurationInterface,
    rbacConfig: any;
    externalUrls: any;
    myHost: string;
    redisConfig: any;
    senderMail: any;
}
