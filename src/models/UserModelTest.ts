import * as Mongoose from "mongoose";
import { UserInterfceT } from "../interfaces/db-interfaces/UserInterfaceT";

export const UserSchemaT = new Mongoose.Schema({
    name: { type: String, required: false, default: '' },
    email: { type: String, required: false, default: '' },
    mobile: { type: String, required: false, default: '' },
    gender: { type: String, required: false, default: '' },

}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});


export const UserModelT = Mongoose.model<UserInterfceT>('userT', UserSchemaT);