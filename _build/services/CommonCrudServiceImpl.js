"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Response_1 = require("../classes/Response");
const humps_1 = require("humps");
const StatusCodes_1 = require("../classes/StatusCodes");
const CustomMessages_1 = require("../classes/CustomMessages");
class CommonCrudServiceImpl {
    constructor(dbModel) {
        this.notReturningKeysInCollection = {
            logs: 0,
            creted_by: 0,
            updated_by: 0,
            created_at: 0,
            updated_at: 0,
            v: 0,
            password: 0,
            is_deleted: 0,
            last_otp: 0,
            is_verified: 0,
            random_key: 0
        };
        this.dbModel = dbModel;
    }
    camalizeKeysForMongoArray(array) {
        return __awaiter(this, void 0, void 0, function* () {
            return array.map(function (question) {
                let a = question.toJSON();
                let id = a['_id'];
                let b = humps_1.camelizeKeys(a);
                b['id'] = id;
                return b;
            });
        });
    }
    createEntry(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let cretaedEntry = yield this.dbModel.create(humps_1.decamelizeKeys(payload));
                let condition = {};
                condition['_id'] = cretaedEntry['_id'];
                let list = yield this.dbModel.find(condition);
                list = yield this.camalizeKeysForMongoArray(list);
                response = new Response_1.Response(true, StatusCodes_1.StatusCodes.CREATED, CustomMessages_1.CustomMessages.CREATED, list);
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return response;
        });
    }
    createOrFindEntry(payload, condition) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let entry = yield this.dbModel.find(condition);
                if (entry.length === 0) {
                    let cretaedEntry = yield this.dbModel.create(humps_1.decamelizeKeys(payload));
                    let resourceEntry = yield this.dbModel.find(condition);
                    resourceEntry = yield this.camalizeKeysForMongoArray(resourceEntry);
                    response = new Response_1.Response(true, StatusCodes_1.StatusCodes.CREATED, CustomMessages_1.CustomMessages.CREATED, resourceEntry);
                }
                else {
                    response = new Response_1.Response(false, StatusCodes_1.StatusCodes.NOT_ACCEPTABLE, CustomMessages_1.CustomMessages.ALREADY_PRESENT, {});
                }
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return response;
        });
    }
    deleteEntry(id, logObj) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let query = { "_id": id };
                let isEntryPresent = yield this.dbModel.findById(id);
                if (!isEntryPresent) {
                    response = new Response_1.Response(false, StatusCodes_1.StatusCodes.BAD_REQUEST, CustomMessages_1.CustomMessages.NOT_PRESENT, {});
                }
                else {
                    let updated_obj = {
                        "$set": {
                            "is_deleted": 1
                        }
                    };
                    if (isEntryPresent['logs'] !== undefined && Object.keys(logObj).length != 0) {
                        let logs = isEntryPresent['logs'];
                        logs.push(logObj);
                        updated_obj['logs'] = logs;
                    }
                    else {
                        updated_obj['logs'].push(logObj);
                    }
                    yield this.dbModel.update(query, updated_obj);
                    response = new Response_1.Response(true, StatusCodes_1.StatusCodes.OK, CustomMessages_1.CustomMessages.DELETED, {});
                }
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return response;
        });
    }
    getAllEntries(query, queryWhereCondition) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let sort = "";
                if (query['sort'])
                    sort = humps_1.decamelize(query['sort']);
                else
                    sort = '-created_at';
                let length = yield this.dbModel.find(humps_1.decamelizeKeys(queryWhereCondition)).countDocuments();
                if (length !== 0) {
                    let list = yield this.dbModel.find(humps_1.decamelizeKeys(queryWhereCondition), this.notReturningKeysInCollection);
                    list = yield this.camalizeKeysForMongoArray(list);
                    let obj = {};
                    obj['list'] = list;
                    obj['count'] = length;
                    response = new Response_1.Response(true, StatusCodes_1.StatusCodes.OK, CustomMessages_1.CustomMessages.SUCCESS, obj);
                }
                else {
                    let obj = {};
                    obj['list'] = [];
                    obj['count'] = 0;
                    response = new Response_1.Response(true, StatusCodes_1.StatusCodes.OK, CustomMessages_1.CustomMessages.SUCCESS, obj);
                }
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return response;
        });
    }
    getSingleEntry(id, keys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (keys && keys.length > 0) {
                for (const key of keys) {
                    this.notReturningKeysInCollection[key] = 0;
                }
            }
            let response;
            try {
                let condition = {};
                condition['_id'] = id;
                let list = yield this.dbModel.find(condition, this.notReturningKeysInCollection);
                list = yield this.camalizeKeysForMongoArray(list);
                response = new Response_1.Response(true, StatusCodes_1.StatusCodes.OK, CustomMessages_1.CustomMessages.SUCCESS, list);
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return response;
        });
    }
    updateEntry(id, payload, logObj) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let condition = {};
                condition['_id'] = id;
                let isEntryPresent = yield this.dbModel.find((condition));
                if (isEntryPresent.length !== 0) {
                    if (isEntryPresent[0].logs !== undefined && Object.keys(logObj).length != 0) {
                        let logs = isEntryPresent[0].logs;
                        if (logObj.email) {
                            logs.push(logObj);
                        }
                        payload['logs'] = logs;
                    }
                    else {
                        if (logObj.email) {
                            payload['logs'] = [];
                            payload['logs'].push(logObj);
                        }
                    }
                    let a = yield this.dbModel.updateOne(condition, humps_1.decamelizeKeys(payload));
                    let updatedEntry = yield this.dbModel.find(condition, this.notReturningKeysInCollection);
                    updatedEntry = yield this.camalizeKeysForMongoArray(updatedEntry);
                    response = new Response_1.Response(true, StatusCodes_1.StatusCodes.OK, CustomMessages_1.CustomMessages.UPDATED, updatedEntry);
                }
                else {
                    response = new Response_1.Response(false, StatusCodes_1.StatusCodes.BAD_REQUEST, CustomMessages_1.CustomMessages.NOT_PRESENT, {});
                }
            }
            catch (err) {
                console.log('error', err);
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return response;
        });
    }
    updateUserEntry(id, payload) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let condition = {};
                condition['_id'] = id;
                let isEntryPresent = yield this.dbModel.find((condition));
                if (isEntryPresent.length !== 0) {
                    let a = yield this.dbModel.updateOne(condition, humps_1.decamelizeKeys(payload));
                    let updatedEntry = yield this.dbModel.find(condition, this.notReturningKeysInCollection);
                    updatedEntry = yield this.camalizeKeysForMongoArray(updatedEntry);
                    response = new Response_1.Response(true, StatusCodes_1.StatusCodes.OK, CustomMessages_1.CustomMessages.UPDATED, updatedEntry);
                }
                else {
                    response = new Response_1.Response(false, StatusCodes_1.StatusCodes.BAD_REQUEST, CustomMessages_1.CustomMessages.NOT_PRESENT, {});
                }
            }
            catch (err) {
                console.log('error', err);
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return response;
        });
    }
}
exports.default = CommonCrudServiceImpl;
//# sourceMappingURL=CommonCrudServiceImpl.js.map