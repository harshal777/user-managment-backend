"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Validations_1 = require("../validations/Validations");
const Response_1 = require("../classes/Response");
const StatusCodes_1 = require("../classes/StatusCodes");
const UserControllerT_1 = require("../controllers/UserControllerT");
function default_1(server) {
    const controller = new UserControllerT_1.default(server);
    server.bind(controller);
    server.route({
        method: 'POST',
        path: '/add-user',
        options: {
            handler: controller.createUser,
            tags: ['api'],
            description: 'Create User Api',
            auth: false,
            validate: {
                payload: Validations_1.userCreationModelT,
                failAction: (request, h, err) => __awaiter(this, void 0, void 0, function* () {
                    if (err) {
                        let response = new Response_1.Response(false, StatusCodes_1.StatusCodes.NOT_ACCEPTABLE, err.message, {});
                        return h.response(response).code(response.getStatusCode()).takeover();
                    }
                    else {
                        return h.continue;
                    }
                })
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            'description': 'user Created.'
                        },
                        '406': {
                            'description': 'Validation Error.'
                        }
                    }
                }
            }
        }
    });
    server.route({
        method: 'get',
        path: '/get-all-users',
        options: {
            handler: controller.getAllUsers,
            tags: ['api'],
            description: 'Get users Api',
            auth: false,
            validate: {
                failAction: (request, h, err) => __awaiter(this, void 0, void 0, function* () {
                    if (err) {
                        let response = new Response_1.Response(false, StatusCodes_1.StatusCodes.NOT_ACCEPTABLE, err.message, {});
                        return h.response(response).code(response.getStatusCode()).takeover();
                    }
                    else {
                        return h.continue;
                    }
                })
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            'description': 'Resource fetched.'
                        },
                        '406': {
                            'description': 'Validation Error.'
                        }
                    }
                }
            }
        }
    });
    server.route({
        method: 'PUT',
        path: '/update-user/{id}',
        options: {
            handler: controller.updateUser,
            tags: ['api'],
            description: 'Update admin Api',
            auth: false,
            validate: {
                params: Validations_1.idInParmsModel,
                failAction: (request, h, err) => __awaiter(this, void 0, void 0, function* () {
                    if (err) {
                        let response = new Response_1.Response(false, StatusCodes_1.StatusCodes.NOT_ACCEPTABLE, err.message, {});
                        return h.response(response).code(response.getStatusCode()).takeover();
                    }
                    else {
                        return h.continue;
                    }
                })
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            'description': 'user role updated Updated.'
                        },
                        '406': {
                            'description': 'Validation Error.'
                        }
                    }
                }
            }
        }
    });
}
exports.default = default_1;
//# sourceMappingURL=UsersRoutesT.js.map