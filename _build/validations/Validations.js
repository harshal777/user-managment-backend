"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userCreationModelT = exports.idInParmsModel = void 0;
const Joi = require("joi");
exports.idInParmsModel = Joi.object().keys({
    id: Joi.string().required()
});
exports.userCreationModelT = Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required(),
    mobile: Joi.string().required(),
    gender: Joi.string().required(),
});
//# sourceMappingURL=Validations.js.map