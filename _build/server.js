"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.init = void 0;
const Hapi = require("@hapi/hapi");
const Response_1 = require("./classes/Response");
const StatusCodes_1 = require("./classes/StatusCodes");
const CustomMessages_1 = require("./classes/CustomMessages");
const UsersRoutesT_1 = require("./routes/UsersRoutesT");
function init(serverConfig, globalVariables) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const port = serverConfig.port;
            const server = new Hapi.Server({
                host: "0.0.0.0",
                port: port,
                router: {
                    stripTrailingSlash: true,
                },
                routes: {
                    cors: {
                        origin: ['*'],
                        credentials: true
                    }
                },
                state: {
                    strictHeader: false
                }
            });
            if (serverConfig.routePrefix) {
                server.realm.modifiers.route.prefix = serverConfig.routePrefix;
            }
            // Plugin Registration
            let hostString = global['host'];
            console.log("host string", hostString);
            yield server.register([
                require('@hapi/inert'),
                require('@hapi/vision')
            ]);
            // await Promise.all(pluginPromises);
            console.log('Register Routes');
            const scheme = function (server, options) {
                return {
                    api: {
                        settings: {
                            x: 5
                        }
                    },
                    authenticate: function (request, h) {
                        return __awaiter(this, void 0, void 0, function* () {
                            let token;
                            // check for basic auth header
                            if (!request.headers.authorization) {
                                let response = new Response_1.Response(false, StatusCodes_1.StatusCodes.UNAUTHORIZED, CustomMessages_1.CustomMessages.MISSING_AUTH_HEADER, {});
                                return h.response(response).code(response.getStatusCode()).takeover();
                            }
                        });
                    }
                };
            };
            server.auth.scheme('custom', scheme);
            server.auth.strategy('default', 'custom');
            server.auth.default('default');
            // Routes Registration
            UsersRoutesT_1.default(server);
            console.log('Routes registered sucessfully.');
            return server;
        }
        catch (err) {
            console.log('Error starting server: ', err);
            throw err;
        }
    });
}
exports.init = init;
//# sourceMappingURL=server.js.map