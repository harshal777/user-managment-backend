"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Server = require("./server");
const Configs = require("./configurations");
const Mongoose = require("mongoose");
const UserModelTest_1 = require("./models/UserModelTest");
console.log(`Running enviroment ${process.env.NODE_ENV || 'development'}`);
// Catch unhandling unexpected exceptions
process.on('uncaughtException', (error) => {
    console.error(`uncaughtException ${error}`);
});
// Catch unhandling rejected promises
process.on('unhandledRejection', (reason) => {
    console.error(`unhandledRejection ${reason}`);
});
const allConfigurations = Configs.getConfigurations();
function initMongoDb(config) {
    Mongoose.Promise = Promise;
    Mongoose.connect(config.connectionString, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true });
    Mongoose.set('debug', function (coll, method, query, doc) { });
    Mongoose.set('useCreateIndex', true);
    let mongoDb = Mongoose.connection;
    mongoDb.on('error', () => {
        console.log(`Unable to connect to database: ${config.connectionString}`);
    });
    mongoDb.once('open', () => {
        console.log(`Connected to database: ${config.connectionString}`);
    });
    return {
        UserModelT: UserModelTest_1.UserModelT,
    };
}
// console.log("All Configuration", allConfigurations);
// Define async start function
const start = ({ config, globalVariables }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const server = yield Server.init(config, globalVariables);
        yield server.start();
        console.log('Server running at:', server.info.uri);
    }
    catch (err) {
        console.error('Error starting server: ', err.message);
        throw err;
    }
});
global['mongo'] = initMongoDb(allConfigurations.mongoConfig);
global['host'] = allConfigurations.myHost;
global['rbacConfig'] = allConfigurations.rbacConfig;
const serverConfigs = allConfigurations.serverConfig;
// Start the server
start({
    config: serverConfigs,
    globalVariables: global
});
//# sourceMappingURL=index.js.map