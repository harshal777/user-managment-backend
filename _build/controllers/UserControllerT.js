"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const CommonCrudServiceImpl_1 = require("../services/CommonCrudServiceImpl");
const StatusCodes_1 = require("../classes/StatusCodes");
const Response_1 = require("../classes/Response");
const CustomMessages_1 = require("../classes/CustomMessages");
class UserController {
    constructor(server) {
        let mongo = global["mongo"];
        this.userService = new CommonCrudServiceImpl_1.default(mongo.UserModelT);
    }
    createUser(request, h) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let condition = {};
                condition["email"] = request.payload["email"];
                response = yield this.userService.createOrFindEntry(request.payload, condition);
                let result = response.getResult();
                if (result && result.length > 0) {
                    response = new Response_1.Response(true, StatusCodes_1.StatusCodes.OK, CustomMessages_1.CustomMessages.SUCCESS, result);
                }
                else {
                    response = new Response_1.Response(false, StatusCodes_1.StatusCodes.UNAUTHORIZED, CustomMessages_1.CustomMessages.ALREADY_PRESENT, {});
                }
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return h.response(response).code(response.getStatusCode());
        });
    }
    getAllUsers(request, h) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                let queryWhereCondition = {};
                response = yield this.userService.getAllEntries(request.query, queryWhereCondition);
                let result = response.getResult();
                if (result['list'] && result['list'].length !== 0) {
                    response.setResult(result);
                }
                else {
                    response.setResult([]);
                }
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return h.response(response).code(response.getStatusCode());
        });
    }
    updateUser(request, h) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                const isExist = yield this.userService.getSingleEntry(request.params['id']);
                if (isExist && isExist.getResult() && isExist.getResult().length > 0) {
                    const payload = request.payload;
                    response = yield this.userService.updateUserEntry(request.params['id'], payload);
                }
                else {
                    response = new Response_1.Response(false, StatusCodes_1.StatusCodes.BAD_REQUEST, CustomMessages_1.CustomMessages.NOT_PRESENT, {});
                }
            }
            catch (err) {
                response = new Response_1.Response(false, StatusCodes_1.StatusCodes.INTERNAL_SERVER_ERROR, err.message, {});
            }
            return h.response(response).code(response.getStatusCode());
        });
    }
}
exports.default = UserController;
//# sourceMappingURL=UserControllerT.js.map