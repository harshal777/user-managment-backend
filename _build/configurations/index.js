"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConfigurations = void 0;
const nconf = require("nconf");
const path = require("path");
//Read Configurations
const configs = new nconf.Provider({
    env: true,
    argv: true,
    store: {
        type: 'file',
        file: path.join(__dirname, `./config.${process.env.NODE_ENV || "local"}.json`)
    }
});
// Return Database configurations
function getConfigurations() {
    return configs.get("configurations");
}
exports.getConfigurations = getConfigurations;
//# sourceMappingURL=index.js.map