"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModelT = exports.UserSchemaT = void 0;
const Mongoose = require("mongoose");
exports.UserSchemaT = new Mongoose.Schema({
    name: { type: String, required: false, default: '' },
    email: { type: String, required: false, default: '' },
    mobile: { type: String, required: false, default: '' },
    gender: { type: String, required: false, default: '' },
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});
exports.UserModelT = Mongoose.model('userT', exports.UserSchemaT);
//# sourceMappingURL=UserModelTest.js.map