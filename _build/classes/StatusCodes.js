"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusCodes = void 0;
class StatusCodes {
}
exports.StatusCodes = StatusCodes;
StatusCodes.OK = 200;
StatusCodes.CREATED = 201;
StatusCodes.BAD_REQUEST = 400;
StatusCodes.UNAUTHORIZED = 401;
StatusCodes.NOT_FOUND = 404;
StatusCodes.NOT_ACCEPTABLE = 406;
StatusCodes.CONFLICT = 409;
StatusCodes.NOT_VERIFIED = 412;
StatusCodes.TOO_MANY_REQUESTS_ERROR = 429;
StatusCodes.INTERNAL_SERVER_ERROR = 500;
//# sourceMappingURL=StatusCodes.js.map