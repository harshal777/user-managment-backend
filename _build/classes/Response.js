"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Response = void 0;
class Response {
    constructor(isSuccess, statusCodes, message, result) {
        this.statusCode = statusCodes;
        this.message = message;
        this.result = result;
        this.isSuccess = isSuccess;
    }
    getIsSuccess() {
        return this.isSuccess;
    }
    getStatusCode() {
        return this.statusCode;
    }
    getResult() {
        return this.result;
    }
    setResult(result) {
        return this.result = result;
    }
}
exports.Response = Response;
//# sourceMappingURL=Response.js.map