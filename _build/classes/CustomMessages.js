"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomMessages = void 0;
class CustomMessages {
}
exports.CustomMessages = CustomMessages;
// Error Messages
CustomMessages.SERVER_ERROR = "Something went wrong, Please contact administrator";
CustomMessages.UNAUTHORIZED = "Unauthorized: Access is denied due to invalid credentials.";
CustomMessages.SUCCESS = "Success";
CustomMessages.NOT_PRESENT = "Entry not present";
CustomMessages.ALREADY_PRESENT = "Entry already present";
CustomMessages.CREATED = "Entry created successfully";
CustomMessages.UPDATED = "Entry updated successfully";
CustomMessages.DELETED = "Entry deleted successfully";
CustomMessages.MISSING_AUTH_HEADER = "Missing Authorization Header";
CustomMessages.UNAUTHORIZED_USER = "Unauthorized user";
CustomMessages.INVALID_BASE64 = "Invalid base64 format'";
CustomMessages.USER_PRESENT = "User/Email already present";
CustomMessages.NOT_VERIFIED = "Please verify user email id";
CustomMessages.SENT_EMAIL = "Verification email has been sent to your maild id please check.";
//# sourceMappingURL=CustomMessages.js.map